package main

import (
	"html/template"
	"os"
)

type User struct {
	Name          string
	Dog           string
	Age           int
	TestId        int
	TestName      string
	Date          string
	Weight        float64
	Height        float64
	LowerPressure int
	UpperPressure int
	Mood         []string
	Misc           map[string]string
}

func main() {

	t, err := template.ParseFiles("hello.gohtml")
	if err != nil {
		panic(err)
	}

	data := User{
		Name:          "Florence",
		Dog:           "Merlin",
		Age:           2,
		TestId:        000456,
		TestName:      "Duke Monster",
		Date:          "Today",
		Height:        5.8,
		Weight:        190,
		LowerPressure: 79,
		UpperPressure: 117,
		Mood:         []string{"cold", "neutral", "sunny"},
		Misc: map[string]string{
			"Eyes":        "Brown",
			"Hand":        "Left",
			"Foot":        "Right",
			"LocationTag": "NWW238",
		},
	}

	err = t.Execute(os.Stdout, data)
	if err != nil {
		panic(err)
	}

}
